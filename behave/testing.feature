Feature: Fight or flight
    In order to increase the ninja survival rate,
    As a ninja commander
    I want my ninjas to decide whether to take on an
    opponent based on their skill levels

    @slow
    Scenario: Weaker opponent
        Given the ninja has a third level black-belt
        When attacked by a samurai
            Then the ninja should engage the opponent

    Scenario: Stronger opponent
        Given the ninja has a third level black-belt
        When attacked by Chuck Norris
            Then the ninja should run for his life

    Scenario: some scenario
        Given a set of specific users
            | name      | department  |
            | Barry     | Beer Cans   |
            | Pudey     | Silly Walks |
            | Two-Lumps | Silly Walks |
        When we count the number of people in each department
            Then we will find two people in "Silly Walks"
            But we will find one person in "Beer Cans"


    Scenario: Search for an account
        Given I search for a valid account
            Then I will see the account details


    Scenario: look up a book
        Given I search for a valid book
            Then the result page will include "success"
    Scenario: look up an invalid book
        Given I search for a invalid book
            Then the result page will include "failure"



