from behave import register_type, given
import parse

# -- TYPE CONVERTER: For a simple, positive integer number.
@parse.with_pattern(r"\d+")
def parse_number(text):
    return int(text)

# -- REGISTER TYPE-CONVERTER: With behave
register_type(Number=parse_number)

# -- STEP DEFINITIONS: Use type converter.
@given('{amount:Number} vehicles')
def step_impl(context, amount):
    assert isinstance(amount, int)

################################################################################

@given('a set of specific users')
def step_impl(context):
    for row in context.table:
        model.add_user(name=row['name'], department=row['department'])

################################################################################

@given('I search for a valid account')
def step_impl(context):
   context.browser.get('http://localhost:8000/index')
   form = get_element(context.browser, tag='form')
   get_element(form, name="msisdn").send_keys('61415551234')
   form.submit()

@then('I will see the account details')
def step_impl(context):
   elements = find_elements(context.browser, id='no-account')
   eq_(elements, [], 'account not found')
   h = get_element(context.browser, id='account-head')
   ok_(h.text.startswith("Account 61415551234"),
       'Heading %r has wrong text' % h.text)

################################################################################

@then('the result page will include "{text}"')
def step_impl(context, text):
   if text not in context.response:
       fail('%r not in %r' % (text, context.response))

################################################################################

@given('I request a new widget for an account via SOAP')
def step_impl(context):
    client = Client("http://127.0.0.1:8000/soap/")
    context.response = client.Allocate(customer_first='Firstname',
        customer_last='Lastname', colour='red')

@then('I should receive an OK SOAP response')
def step_impl(context):
    eq_(context.response['ok'], 1)

