Feature: Demonstrate how to test Django with behave & mechanize
    Scenario Outline: Check auth
        Given I put <user> in a blender,
        When I log in
            Then I see my account summary
            And I see a warm and welcoming message
            And it should transform into <fullname> 

        Examples: Identities
            | user         | fullname        | email          |
            | shivha       | TAYAA Med Amine | amine@tayaa.me |

    Scenario: Check auth
        Given a user
        When I log in
            Then I see my account summary
            And I see a warm and welcoming message

